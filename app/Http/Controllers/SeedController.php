<?php

namespace App\Http\Controllers;

use App\Models\Seed;
use Illuminate\Http\Request;

class SeedController extends Controller
{
    public function showAll()
    {
        return response()->json(Seed::all());
    }

    public function show($id)
    {
        return response()->json(Seed::find($id));
    }

    public function create(Request $request)
    {
        $seed = Seed::create($request->all());

        return response()->json($seed, 201);
    }

    public function update($id, Request $request)
    {
        $seed = Seed::findOrFail($id);
        $seed->update($request->all());

        return response()->json($seed, 200);
    }

    public function delete($id)
    {
        Seed::findOrFail($id)->delete();
        return response(204);
    }
}
