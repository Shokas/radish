<?php

namespace App\Http\Controllers;

use App\Models\Tray;
use Illuminate\Http\Request;

class TrayController extends Controller
{
    public function showAll()
    {
        return response()->json(Tray::all());
    }

    public function show($id)
    {
        return response()->json(Tray::find($id));
    }

    public function create(Request $request)
    {
        $tray = Tray::create($request->all());

        return response()->json($tray, 201);
    }

    public function update($id, Request $request)
    {
        $tray = Tray::findOrFail($id);
        $tray->update($request->all());

        return response()->json($tray, 200);
    }

    public function delete($id)
    {
        Tray::findOrFail($id)->delete();
        return response(204);
    }
}
