<?php

namespace App\Http\Controllers;

use App\Models\GrowingMedium;
use Illuminate\Http\Request;

class GrowingMediumController extends Controller
{
    public function showAll()
    {
        return response()->json(GrowingMedium::all());
    }

    public function show($id)
    {
        return response()->json(GrowingMedium::find($id));
    }

    public function create(Request $request)
    {
        $growingMedium = GrowingMedium::create($request->all());

        return response()->json($growingMedium, 201);
    }

    public function update($id, Request $request)
    {
        $growingMedium = GrowingMedium::findOrFail($id);
        $growingMedium->update($request->all());

        return response()->json($growingMedium, 200);
    }

    public function delete($id)
    {
        GrowingMedium::findOrFail($id)->delete();
        return response(204);
    }
}
