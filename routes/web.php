<?php

/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->group(['prefix' => 'api'], function () use ($router) {
    $router->get('trays',  ['uses' => 'TrayController@showAll']);
    $router->get('trays/{id}', ['uses' => 'TrayController@show']);
    $router->post('trays', ['uses' => 'TrayController@create']);
    $router->delete('trays/{id}', ['uses' => 'TrayController@delete']);
    $router->put('trays/{id}', ['uses' => 'TrayController@update']);

    $router->get('gm',  ['uses' => 'GrowingMediumController@showAll']);
    $router->get('gm/{id}', ['uses' => 'GrowingMediumController@show']);
    $router->post('gm', ['uses' => 'GrowingMediumController@create']);
    $router->delete('gm/{id}', ['uses' => 'GrowingMediumController@delete']);
    $router->put('gm/{id}', ['uses' => 'GrowingMediumController@update']);

    $router->get('seed',  ['uses' => 'SeedController@showAll']);
    $router->get('seed/{id}', ['uses' => 'SeedController@show']);
    $router->post('seed', ['uses' => 'SeedController@create']);
    $router->delete('seed/{id}', ['uses' => 'SeedController@delete']);
    $router->put('seed/{id}', ['uses' => 'SeedController@update']);
});